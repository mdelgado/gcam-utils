#!/usr/bin/env python
'''
Created on 2/12/15

@author: Rich Plevin (rich@plevin.com)

Simplified interface for running batch queries against GCAM's XML database to
generate CSV files. Generates an XML batch query file based on command-line
arguments and a configuration file which stores information about the user's
GCAM setup, e.g., the location of the folder containing the Main_User_Workspace.

This functionality (and more) is available in the gcammcs framework, but the
advantage of this script is that it requires only standard Python modules.
'''

# Copyright (c) 2015, Richard Plevin.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os
import subprocess
import sys
import argparse
import string
import re

# Read the following imports from the same dir as the script
sys.path.insert(0, os.path.dirname(sys.argv[0]))

from common import readConfigFiles, getParam, getParamAsBoolean, getTempFile, GCAM_32_REGIONS, readRegionMap
from Xvfb import Xvfb

PROGRAM = os.path.basename(__file__)
VERSION = "0.95"


def parseArgs():
    parser = argparse.ArgumentParser(
        prog=PROGRAM,
        fromfile_prefix_chars='@',      # use "@" before value to substitute contents of file as arguments
        description='''
    Run one or more GCAM database queries by generating and running the
    named XML queries. The results are placed in a file in the specified
    output directory with a name composed of the basename of the
    XML query file plus the scenario name. For example,
    "batchQuery.py -o. -s MyReference,MyPolicyCase liquids-by-region"
    would generate query results into the files ./liquids-by-region-MyReference.csv
    and ./liquids-by-region-MyPolicyCase.csv.

    The named queries are located using the value of config variable GCAM.QueryPath,
    which can be overridden with the -Q argument. The QueryPath consists of one or
    more colon-delimited elements that can identify directories or XML files. The
    elements of QueryPath are searched in order until the named query is found. If
    a path element is a directory, the filename composed of the query + '.xml' is
    sought in that directory. If the path element is an XML file, a query with a
    title matching the query name (first literally, then by replacing '_' and '-'
    characters with spaces) is sought. Note that query names are case-sensitive.

    This script populates an initial
    configuration file in ~/.gcam.cfg when first run. The config file should be
    customized as needed, e.g., to set "GcamRoot" to the directory holding your
    Main_User_Workspace unless it happens to live in ~/GCAM, which is the default value.''')

    # REQUIRED
    parser.add_argument('queryName', type=str, nargs='*',
                        help='''A file or files, each holding an XML query to run. (The ".xml" suffix will be added if needed.)
                                If an argument is preceded by the "@" sign, it is read and its contents substituted as the
                                values for this argument. That means you can store queries to run in a file (one per line) and
                                just reference the file by preceding the filename argument with "@".''')

    parser.add_argument('-d', '--xmldb', type=str,
                         help='''The XML database to query (default is value of GCAM.DbFile, in the GCAM.Workspace's
                         "output" directory. Overrides the -w flag.''')

    parser.add_argument('-D', '--dontDelete', action="store_true",
                        help='''Don't delete any temporary file created by extracting a query from a query file. Used
                                mainly for debugging.''')

    parser.add_argument('-R', '--regionMap', type=str,
                        help='''A file containing tab-separated pairs of names, the first being a GCAM region
                                and the second being the name to map this region to. Lines starting with "#" are
                                treated as comments. Lines without a tab character are also ignored. This arg
                                overrides the value of config variable GCAM.RegionMapFile.''')

    parser.add_argument('-n', '--noRun', action="store_true",
                        help="Show the command to be run, but don't run it")

    parser.add_argument('-o', '--outputDir', type=str,
                         help='Where to output the result (default taken from config parameter "GCAM.OutputDir")')

    parser.add_argument('-Q', '--queryPath', type=str, default=None,
                        help='''A colon-delimited list of directories or filenames to look in to find query files.
                                Defaults to value of config parameter GCAM.QueryPath''')

    parser.add_argument('-r', '--regions', type=str, default=None,
                        help='''A comma-separated list of regions on which to run queries found in query files structured
                                like Main_Queries.xml. If not specified, defaults to querying all 32 regions.''')

    parser.add_argument('-s', '--scenario', type=str, default='Reference',
                        help='''A comma-separated list of scenarios to run the query/queries for (default is "Reference")
                                Note that these refer to a scenarios in the XML database.''')

    parser.add_argument('-v', '--verbose', action='count',
                        help="Show command being executed.")

    parser.add_argument('-V', '--version', action='version', version='%(prog)s ' + VERSION,
                        help="Show version number and exit.")

    parser.add_argument('-w', '--workspace', type=str, default='',
                        help='''The workspace directory in which to find the XML database.
                                Defaults to value of config file parameter GCAM.Workspace.
                                Overridden by the -d flag.''')

    args = parser.parse_args()

    return args

BatchQueryTemplate = string.Template("""<?xml version="1.0"?>
<!-- WARNING: this file is automatically generated. Changes may be overwritten. -->
<ModelInterfaceBatch>
    <class name="ModelInterface.ModelGUI2.DbViewer">
        <command name="XMLDB Batch File">
            <scenario name="${scenario}"/>
            <queryFile>${queryFile}</queryFile>
            <outFile>${csvFile}</outFile>
            <xmldbLocation>${xmldb}</xmldbLocation>
            <batchQueryResultsInDifferentSheets>false</batchQueryResultsInDifferentSheets>
            <batchQueryIncludeCharts>false</batchQueryIncludeCharts>
            <batchQuerySplitRunsInDifferentSheets>false</batchQuerySplitRunsInDifferentSheets>
            <batchQueryReplaceResults>true</batchQueryReplaceResults>
        </command>
    </class>
</ModelInterfaceBatch>
""")


def findOrCreateQueryFile(title, queryPath, regions, regionMap=None):
    '''
    Find a query with the given title either as a file (with .xml extension) or
    within an XML query file by searching queryPath. If the query with "title" is
    found in an XML query file, extract it to generate a batch query file and
    apply it to the given regions.
    '''
    items = queryPath.split(':')
    for item in items:
        if os.path.isdir(item):
            pathname = os.path.join(item, title + '.xml')
            if os.path.isfile(pathname):
                return (pathname, False)
        else:
            from lxml import etree as ET    # lazy import speeds startup

            tree = ET.parse(item)
            xpath = '/queries/queryGroup/*[@title="%s"]' % title
            elts = tree.xpath(xpath)  # returns empty list or list of elements found

            if elts is None or len(elts) == 0:
                # if the literal search fails, repeat search with all "-" or "_" changed to " ".
                xpath = '/queries/queryGroup/*[@title="%s"]' % re.sub('_', ' ', title)
                elts = tree.xpath(xpath)

            if elts is None or len(elts) == 0:
                # if the literal search fails, repeat search with all "-" or "_" changed to " ".
                xpath = '/queries/queryGroup/*[@title="%s"]' % re.sub('-', ' ', title)
                elts = tree.xpath(xpath)

            if elts is None or len(elts) == 0:
                # if the literal search fails, repeat search with all "-" or "_" changed to " ".
                xpath = '/queries/queryGroup/*[@title="%s"]' % re.sub('[-_]', ' ', title)
                elts = tree.xpath(xpath)

            if len(elts) == 1:
                elt = elts[0]
                root = ET.Element("queries")
                aQuery = ET.Element("aQuery")
                root.append(aQuery)
                for region in regions:
                    aQuery.append(ET.Element('region', name=region))

                aQuery.append(elt)

                if regionMap:
                    # if a rewrite list exists already, use it, otherwise create it.
                    subtree = ET.ElementTree(element=elt)
                    found = subtree.xpath('//labelRewriteList')
                    if len(found) == 1:
                        rewriteList = found[0]
                    else:
                        # create and inject the element
                        rewriteList = ET.Element('labelRewriteList')
                        elt.append(rewriteList)

                    level = ET.Element('level', name='region')
                    rewriteList.append(level)
                    for fromReg, toReg in regionMap.iteritems():
                        level.append(ET.Element('rewrite', attrib={'from': fromReg, 'to': toReg}))

                tmpFile = getTempFile('.xml')
                print "Writing extracted query for '%s' to tmp file '%s'" % (title, tmpFile)
                tree = ET.ElementTree(root)
                tree.write(tmpFile, xml_declaration=True, encoding="UTF-8", pretty_print=True)
                return (tmpFile, True)

    return (None, False)


def main():
    args = parseArgs()
    readConfigFiles()

    jarFile     = getParam('GCAM.JarFile')
    javaLibPath = getParam('GCAM.JavaLibPath')
    javaArgs    = getParam('GCAM.JavaArgs')
    miLogFile   = getParam('GCAM.ModelInterfaceLogFile')
    outputDir   = args.outputDir or getParam('GCAM.OutputDir')
    workspace   = args.workspace or getParam('GCAM.Workspace')
    xmldb       = args.xmldb     or os.path.join(workspace, 'output', getParam('GCAM.DbFile'))
    queryPath   = args.queryPath or getParam('GCAM.QueryPath')
    regionFile  = args.regionMap or getParam('GCAM.RegionMapFile')
    regions     = args.regions.split(',') if args.regions else GCAM_32_REGIONS
    scenarios   = args.scenario.split(',')
    queryNames  = args.queryName

    print "Query names: '%s'" % queryNames

    if not queryNames:
        print "Error: At least one query name must be specified"
        sys.exit(1)

    if not os.path.lexists(outputDir):
        print "Output directory '%s' does not exist" % outputDir
        sys.exit(1)

    xmldb = os.path.abspath(xmldb)

    regionMap = readRegionMap(regionFile) if regionFile else None

    redirect = ""
    if miLogFile:
        miLogFile = os.path.abspath(miLogFile)
        redirect = ">> %s 2>&1" % miLogFile
        if os.path.lexists(miLogFile):
            os.unlink(miLogFile)       # remove it, if any, to start fresh

    for scenario in scenarios:
        for queryName in queryNames:
            queryName = queryName.strip()

            if not queryName or queryName[0] == '#':    # allow blank lines and comments
                continue

            if args.verbose:
                print "\nProcessing query '%s'" % queryName

            basename = os.path.basename(queryName)
            mainPart, extension = os.path.splitext(basename)   # strip extension, if any

            # Look for both the literal name as given as well as the name with underscores replaced with spaces
            filename, isTempFile = findOrCreateQueryFile(basename, queryPath, regions, regionMap=regionMap)

            if not filename:
                print "Error: file for query '%s' was not found." % basename
                sys.exit(1)

            csvFile = "%s-%s.csv" % (mainPart, scenario)
            csvPath = os.path.abspath(os.path.join(outputDir, csvFile))
            csvPath = csvPath.replace(' ', '_')     # eliminate spaces for general convenience

            # Write a batch file for ModelInterface to invoke the query on the named scenario, and put output where specified
            batchFile = getTempFile('.xml')
            batchFileText = BatchQueryTemplate.substitute(scenario=scenario, queryFile=filename, csvFile=csvPath, xmldb=xmldb)

            print "Creating temporary batch file '%s'" % batchFile
            with open(batchFile, "w") as fp:
                fp.write(batchFileText)

            if args.verbose:
                print "Generating %s" % csvPath

            if miLogFile:
                subprocess.call("(echo Query file: '%s'; cat %s) %s" % (filename,  filename,  redirect), shell=True)
                subprocess.call("(echo Batch file: '%s'; cat %s) %s" % (batchFile, batchFile, redirect), shell=True)

            javaLibPathArg = "-Djava.library.path='%s'" % javaLibPath if javaLibPath else ""

            command = "java %s %s -jar '%s' -b '%s' %s" % (javaArgs, javaLibPathArg, jarFile, batchFile, redirect)

            if args.verbose or args.noRun:
                print command

            if not args.noRun:

                useVirtualBuffer = getParamAsBoolean('GCAM.UseVirtualBuffer')

                try:
                    xvfb = None
                    if useVirtualBuffer:
                        xvfb = Xvfb()      # run the X virtual buffer to suppress popup windows

                    subprocess.call(command, shell=True)

                    # The java program always exits with 0 status, but when the query fails,
                    # it writes an error message to the CSV file. If this occurs, we delete
                    # the file.
                    testCommand = "head -1 '%s' | grep -q 'java.lang.Exception:'" % csvPath
                    if subprocess.call(testCommand, shell=True) == 0:
                        print "Query '%s' failed.\nDeleting '%s'" % (queryName, csvPath)
                        os.remove(csvPath)

                except:
                    raise

                finally:        # caller (runGCAM function) traps exceptions, so we just re-raise
                    if args.dontDelete:
                        print "Not deleting tmp batch file '%s'" % batchFile
                    else:
                        print "Deleting tmp batch file '%s'" % batchFile
                        os.remove(batchFile)

                    if xvfb:
                        xvfb.terminate()

                    if isTempFile:
                        if args.dontDelete:
                            print "Not deleting tmp file '%s'" % filename
                        else:
                            print "Deleting tmp file '%s'" % filename
                            os.remove(filename)

if __name__ == '__main__':
    main()
