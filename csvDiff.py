#!/usr/bin/env python
'''
Created on 2/11/15

@author: Rich Plevin (rich@plevin.com)
'''

# Copyright (c) 2015, Richard Plevin.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os
import sys
import argparse
import pandas as pd

# Read the following imports from the same dir as the script
sys.path.insert(0, os.path.dirname(sys.argv[0]))

from common import ensureCSV, ensureXLSX, interpolateYears, limitYears, dropExtraCols

PROGRAM = os.path.basename(__file__)
VERSION = "1.0"

Verbose = False
Years = None            # List of strings representing start and end year of interest, or None
StartYear = None

def parseArgs():
    parser = argparse.ArgumentParser(description='''Compute the differences in the timeseries data in two or
    more CSV files. The "year columns" in reference case results (the first file given) are subtracted from
    those in each of the other result files. Each comparison is labeled and written to the output CSV file given.
    As a convenience, if any of the filenames is given without an extension, the ".csv" extension is added.
    ''')

    #
    # Required arguments
    #
    parser.add_argument('csvFiles', nargs='+',
                        help='''The files to process. For difference operations, the first file is treated
                        as the reference file whose time-series data is subtracted from that of each other
                        file. If missing, ".csv" suffixes are added to all arguments (the ".csv" is optional).''')
    #
    # Optional
    #
    parser.add_argument('-g', '--groupSum', type=str, default="",
                        help='''Group data for each timestep (or interpolated annual values) by the
                        given column, and sum all members of each group to produce a timeseries for
                        each group. Takes precedence over the simpler "-S" ("--sum") option.''')

    parser.add_argument('-i', '--interpolate', action="store_true",
                        help="Interpolate (linearly) annual values between timesteps.")

    parser.add_argument('-o', '--outFile', default='differences.csv',
                        help='''The name of the ".csv" or ".xlsx" file containing the differences
                        between each scenario and the reference. Default is "differences.csv".''')

    parser.add_argument('-c', '--convertOnly', default=False, action="store_true",
                        help='''Convert the given CSV files into an Excel workbook, one sheet per CSV file.''')

    parser.add_argument('-p', '--percentage', default=False, action="store_true",
                        help='''Compute the difference on a percentage basis, i.e.,
                                (X minus reference)/reference.''')

    parser.add_argument('-S', '--sum', default=False, action="store_true",
                        help='''Sum all timestep (or interpolated annual values) to produce a single time-series.''')

    parser.add_argument('-s', '--skiprows', type=int, default=1,
                        help='''The number of rows to skip. Default is 1, which works for GCAM batch query output.
                        Use -s0 for outFile.csv''')

    parser.add_argument('-y', '--years', type=str, default="",
                        help='''Takes a parameter of the form XXXX-YYYY, indicating start and end years of interest.
                        Other years are dropped (except for annual outputs.)''')

    parser.add_argument('-Y', '--startYear', type=int, default=0,
                        help='''The year at which to begin interpolation''')

    parser.add_argument('-v', '--verbose', action="store_true",
                        help="Print diagnostic messages.")

    parser.add_argument('-V', '--version', action='version', version='%(prog)s ' + VERSION,
                        help="Show version number and exit.")

    args = parser.parse_args()

    return args


def readGcamCsv(filename, skiprows=1, interpolate=False, exitOnError=True):
    try:
        df = pd.read_table(filename, sep=',', skiprows=skiprows, index_col=None)
    except Exception, e:
        print "*** Reading file '%s' failed: %s\n" % (filename, e)
        if exitOnError:
            sys.exit(1)
        else:
            raise

    if Years:
        limitYears(df, Years)

    if interpolate:
        if Verbose:
            print "Interpolating data from", filename
        df = interpolateYears(df, startYear=StartYear)
    return df


def computeDifference(df1, df2, percentage=False):
    '''
    Compute the difference of of the contents of filename1 minus contents of filename2,
    returning a new DataFrame with the difference in all the year columns.
    '''
    assert not (df1 is None or df2 is None), \
        "Can't compute difference; one or more DataFrames is None"

    df1 = dropExtraCols(df1, inplace=False)
    dropExtraCols(df2, inplace=True)

    assert set(df1.columns) == set(df2.columns), \
        "Can't compute difference because result sets have different columns. df1:%s, df2:%s" % (df1.columns, df2.columns)

    yearCols = filter(str.isdigit, df1.columns)
    nonYearCols = list(set(df1.columns) - set(yearCols))

    df1.set_index(nonYearCols, inplace=True)
    df2.set_index(nonYearCols, inplace=True)

    # Compute difference for timeseries values
    diff = df2 - df1

    if percentage:
        diff = diff / df1

    return diff


# TBD: Maybe unneeded
def sumYears(files, skiprows=1, interpolate=False):
    csvFiles = map(ensureCSV, files)
    dframes  = map(lambda fname: readGcamCsv(fname, skiprows=skiprows, interpolate=interpolate), csvFiles)

    # TBD: preserve columns that have a single value only? Maybe this collapses into sumYearsByGroup()?
    for df, fname in zip(dframes, csvFiles):
        root, ext = os.path.splitext(fname)
        outFile = root + '-sum' + ext
        yearCols = filter(str.isdigit, df.columns)

        with open(outFile, 'w') as f:
            sums = df[yearCols].sum()
            csvText = sums.to_csv(None)
            label = outFile
            f.write("%s\n%s\n" % (label, csvText))


# TBD: decide whether to pass an output directory
def sumYearsByGroup(groupCol, files, skiprows=1, interpolate=False):
    import numpy as np

    csvFiles = map(ensureCSV, files)
    dframes  = map(lambda fname: readGcamCsv(fname, skiprows=skiprows, interpolate=interpolate), csvFiles)

    for df, fname in zip(dframes, csvFiles):
        units = df['Units'].unique()
        if len(units) != 1:
            raise Exception, "Can't sum results; rows have different units: %s" % units

        root, ext = os.path.splitext(fname)
        name = groupCol.replace(' ', '_')     # eliminate spaces for general convenience
        outFile = '%s-groupby-%s%s' % (root, name, ext)

        cols = [groupCol] + filter(str.isdigit, df.columns)
        grouped = df[cols].groupby(groupCol)
        df2 = grouped.aggregate(np.sum)
        df2['Units'] = units[0]         # add these units to all rows

        with open(outFile, 'w') as f:
            csvText = df2.to_csv(None)
            label = outFile
            f.write("%s\n%s\n" % (label, csvText))


def csv2xlsx(inFiles, outFile, skiprows=0, interpolate=False):
    csvFiles = map(ensureCSV, inFiles)
    # TBD: catch exception on reading bad CSV file; save error and report at the end
    dframes  = map(lambda fname: readGcamCsv(fname, skiprows=skiprows, interpolate=interpolate), csvFiles)

    sheetNum = 1
    outFile = ensureXLSX(outFile)
    with pd.ExcelWriter(outFile, engine='xlsxwriter') as writer:
        workbook = writer.book
        linkFmt = workbook.add_format({'font_color': 'blue', 'underline': True})

        # Create an index sheet
        indexSheet = workbook.add_worksheet('index')
        indexSheet.write_string(0, 1, 'Links to query results')
        maxlen = max(map(len, csvFiles))
        indexSheet.set_column('B:B', maxlen)
        for i, name in enumerate(csvFiles):
            row = i+1
            indexSheet.write(row, 0, row)
            indexSheet.write_url(row, 1, "internal:%d!A1" % row, linkFmt, name)
            #indexSheet.write(row, 1, '=hyperlink("#%d!A1", "%s")' % (row, name), linkFmt)

        for df, fname in zip(dframes, csvFiles):
            sheetName = str(sheetNum)
            sheetNum += 1
            dropExtraCols(df, inplace=True)
            df.to_excel(writer, index=None, sheet_name=sheetName, startrow=3, startcol=0)
            worksheet = writer.sheets[sheetName]
            worksheet.write_string(0, 0, "Filename:")
            worksheet.write_string(0, 1, fname)
            #  =HYPERLINK("#1!A1", "Joe")
            #worksheet.write(1, 0, '=hyperlink("#index!A1", "Back to index")', linkFmt)
            worksheet.write_url(1, 0, "internal:index!A1", linkFmt, "Back to index")


def writeDiffsToCSV(outFile, referenceFile, otherFiles, skiprows=1, interpolate=False, percentage=False):
    refDF = readGcamCsv(referenceFile, skiprows=skiprows, interpolate=interpolate)

    with open(outFile, 'w') as f:
        for otherFile in otherFiles:
            otherFile = ensureCSV(otherFile)   # add csv extension if needed
            otherDF   = readGcamCsv(otherFile, skiprows=skiprows, interpolate=interpolate)

            diff = computeDifference(refDF, otherDF, percentage=percentage)

            csvText = diff.to_csv(None)
            label = "[%s] minus [%s]" % (otherFile, referenceFile)
            if percentage:
                label = "(%s)/%s" % (label, referenceFile)
            f.write("%s\n%s" % (label, csvText))    # csvText has "\n" already


def writeDiffsToXLSX(outFile, referenceFile, otherFiles, skiprows=1, interpolate=False, percentage=False):
    with pd.ExcelWriter(outFile, engine='xlsxwriter') as writer:
        sheetNum = 1
        if Verbose:
            print "Reading reference file", referenceFile
        refDF = readGcamCsv(referenceFile, skiprows=skiprows, interpolate=interpolate)

        for otherFile in otherFiles:
            otherFile = ensureCSV(otherFile)   # add csv extension if needed
            if Verbose:
                print "Reading other file", otherFile
            otherDF   = readGcamCsv(otherFile, skiprows=skiprows, interpolate=interpolate)

            sheetName = 'Diff%d' % sheetNum
            sheetNum += 1

            diff = computeDifference(refDF, otherDF, percentage=percentage)

            diff.reset_index(inplace=True)      # convert multi-index into regular column values
            diff.to_excel(writer, index=None, sheet_name=sheetName, startrow=2, startcol=0)

            #workbook  = writer.book
            #worksheet = workbook.add_worksheet(sheetName)
            worksheet = writer.sheets[sheetName]
            label     = "[%s] minus [%s]" % (otherFile, referenceFile)
            if percentage:
                label = "(%s)/%s" % (label, referenceFile)
            worksheet.write_string(0, 0, label)

            startRow = diff.shape[0] + 4
            worksheet.write_string(startRow, 0, otherFile)
            startRow += 2
            otherDF.reset_index(inplace=True)
            otherDF.to_excel(writer, index=None, sheet_name=sheetName, startrow=startRow, startcol=0)

        dropExtraCols(refDF, inplace=True)
        if Verbose:
            print "writing DF to excel file", outFile
        refDF.to_excel(writer, index=None, sheet_name='Reference', startrow=0, startcol=0)


def main():
    args = parseArgs()

    global Verbose
    Verbose = args.verbose

    csvFiles = map(ensureCSV, args.csvFiles)
    referenceFile = csvFiles[0]
    otherFiles    = csvFiles[1:] if len(csvFiles) > 1 else []

    outFile = args.outFile
    root, ext = os.path.splitext(outFile)
    if not ext:
        outFile = ensureCSV(outFile)
        ext = '.csv'

    extensions = ('.csv', '.xlsx')
    if ext not in extensions:
        print "Output file extension must be one of %s" % extensions
        sys.exit(1)

    percentage  = args.percentage
    convertOnly = args.convertOnly
    skiprows    = args.skiprows
    interpolate = args.interpolate
    groupSum    = args.groupSum
    sum         = args.sum

    yearStrs = args.years.split('-')
    if len(yearStrs) == 2:
        global Years, StartYear
        Years = yearStrs
        StartYear = args.startYear

    if convertOnly or groupSum or sum:
        if convertOnly:
            csv2xlsx(csvFiles, outFile, skiprows=skiprows, interpolate=interpolate)
        elif groupSum:
            sumYearsByGroup(groupSum, csvFiles, skiprows=skiprows, interpolate=interpolate)
        elif sum:
            sumYears(csvFiles, skiprows=skiprows, interpolate=interpolate)
        return

    if ext == '.csv':
        writeDiffsToCSV(outFile, referenceFile, otherFiles, skiprows=skiprows,
                        interpolate=interpolate, percentage=percentage)
    else:
        writeDiffsToXLSX(outFile, referenceFile, otherFiles, skiprows=skiprows,
                         interpolate=interpolate, percentage=percentage)


if __name__ == '__main__':
    try:
        main()
    except Exception, e:
        print "%s failed: %s" % (PROGRAM, e)
        sys.exit(1)
