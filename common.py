'''
Created on: 2/12/15

@author: Rich Plevin (rich@plevin.com)

Functions and data common to GCAM utilities (e.g., batchQuery, queueGCAM, csvDiff)
'''

# Copyright (c) 2015, Richard Plevin.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os
import io
import platform
from ConfigParser import SafeConfigParser

DEFAULT_SECTION = 'DEFAULT'
GCAM_SECTION    = 'GCAM'
USR_CONFIG_FILE = '.gcam.cfg'
CONFIG_VAR_NAME = 'QUEUE_GCAM_CONFIG_FILE'
WORKSPACE_VAR_NAME   = 'QUEUE_GCAM_WORKSPACE'
NO_RUN_GCAM_VAR_NAME = 'QUEUE_GCAM_NO_RUN_GCAM'

#
# These are defaults for the batchQuery and queueGCAM utilities
#

# Stub ~/.gcam.cfg file which user can edit
_UserDefaults = \
"""
[GCAM]
# Add customizations here, e.g., set GCAM.Root to another directory
"""

GCAM_32_REGIONS = [
    'Africa_Eastern',
    'Africa_Northern',
    'Africa_Southern',
    'Africa_Western',
    'Argentina',
    'Australia_NZ',
    'Brazil',
    'Canada',
    'Central America and Caribbean',
    'Central Asia',
    'China',
    'Colombia',
    'EU-12',
    'EU-15',
    'Europe_Eastern',
    'Europe_Non_EU',
    'European Free Trade Association',
    'India',
    'Indonesia',
    'Japan',
    'Mexico',
    'Middle East',
    'Pakistan',
    'Russia',
    'South Africa',
    'South America_Northern',
    'South America_Southern',
    'South Asia',
    'South Korea',
    'Southeast Asia',
    'Taiwan',
    'USA'
]

_SystemDefaults = \
"""
[GCAM]
# Sets the folder holding the symlink "current" which refers
# to a folder holding Main_User_Workspace and ModelInterface.
# (This is one way of setting up the code, but not required.)
GCAM.Root = %(Home)s/GCAM

# Refers to the GCAM folder holding the version of the model
# you want to use. It is convenient to make this a symbolic link.
GCAM.Current = %(GCAM.Root)s/current

# The location of the Main_User_Workspace to use. This can refer
# to any folder; GCAM.Current is just an optional convention.
GCAM.Workspace = %(GCAM.Current)s/Main_User_Workspace

# The location of the ModelInterface to use.
GCAM.ModelInterface = %(GCAM.Current)s/ModelInterface

# The location of the libraries needed by ModelInterface.
# (Not needed if using GCAM with BaseX rather than dbxml.)
GCAM.JavaLibPath = %(GCAM.Workspace)s/libs/dbxml/lib

# Arguments to java to ensure that ModelInterface has enough
# heap space.
GCAM.JavaArgs = -Xms512m -Xmx2g

# The default to which batchQuery.py writes CSV files.
GCAM.OutputDir = %(GCAM.Workspace)s/output

# Name of batch files generated by batchQuery. There should
# be no need to change this, but you can if desired.
GCAM.BatchFile = generated_batch_file.xml

# A string with one or more colon-delimited elements that identify
# directories or XML files in which to find batch query definitions.
GCAM.QueryPath = .

# The name of the queue used for submitting batch jobs on a cluster.
GCAM.DefaultQueue = standard

GCAM.QsubCommand = qsub -q {queueName} -N {jobName} -l walltime={walltime} -d {exeDir} -e {logFile} -m n -j oe -l pvmem=6GB -v QUEUE_GCAM_CONFIG_FILE='{configs}',QUEUE_GCAM_WORKSPACE='{workspace}',QUEUE_GCAM_NO_RUN_GCAM={noRunGCAM}

# --signal=USR1@15 => send SIGUSR1 15s before walltime expires
GCAM.SlurmCommand = sbatch -p {queueName} --nodes=1 -J {jobName} -t {walltime} -D {exeDir} --get-user-env=L -s --mem=6000 --tmp=6000 --export=QUEUE_GCAM_CONFIG_FILE='{configs}',QUEUE_GCAM_WORKSPACE='{workspace}',QUEUE_GCAM_NO_RUN_GCAM={noRunGCAM}

GCAM.BatchCommand = %(GCAM.QsubCommand)s

# Arguments to qsub's "-l" flag that define required resources
GCAM.QsubResources = pvmem=6GB

# Environment variables to pass to qsub. (Not needed by most users.)
GCAM.QsubEnviroVars =

# Default location in which to look for scenario directories
GCAM.ScenariosDir = %(GCAM.Root)s/scenarios

# For qsub, the default number of minutes to allocate per task.
GCAM.Minutes = 20

# Whether to use the "virtual buffer", allowing ModelInterface to
# run without generating pop-up windows on Linux.
GCAM.UseVirtualBuffer = yes

# A script to run by queueGCAM after GCAM completes. The script is
# called with 3 arguments: workspace directory, XML configuration
# file, and scenario name.
GCAM.PostProcessor =

# A file that maps GCAM regions to rename them or to aggregate
# them. Each line consists of a GCAM region name, some number of
# tabs, and the name to map the region to.
GCAM.RegionMapFile =

# Where to create temporary files
GCAM.TempDir = /tmp
"""

#GCAM.OutFilePatterns    = %(GCAM.Root)s/outFilePatterns.txt


_ConfigParser = None

def readConfigFiles():
    global _ConfigParser

    home = os.getenv('HOME')
    platformName = platform.system()

    assert platformName in ('Darwin', 'Linux'), "Only Darwin (OS X) and Linux are supported currently"

    if platformName == 'Darwin':
        jarFile = '%(GCAM.ModelInterface)s/ModelInterface.app/Contents/Resources/Java/ModelInterface.jar'
        exeFile = 'Release/objects'
        useXvfb = 'False'
    elif platformName == 'Linux':
        jarFile = '%(GCAM.ModelInterface)s/ModelInterface.jar'
        exeFile = './gcam.exe'
        useXvfb = 'True'

    # Initialize config parser with default values
    _ConfigParser = SafeConfigParser()
    _ConfigParser.readfp(io.BytesIO(_SystemDefaults))
    _ConfigParser.set(GCAM_SECTION, 'Home', home)
    _ConfigParser.set(GCAM_SECTION, 'GCAM.Executable', exeFile)
    _ConfigParser.set(GCAM_SECTION, 'GCAM.JarFile', jarFile)
    _ConfigParser.set(GCAM_SECTION, 'GCAM.UseVirtualBuffer', useXvfb)

    # Customizations are stored in ~/.gcam.cfg
    usrConfigPath = os.path.join(home, USR_CONFIG_FILE)
    if os.path.exists(usrConfigPath):
        _ConfigParser.readfp(open(usrConfigPath))
    else:
        # create an empty file with the [GCAM] section if no file exists
        with open(usrConfigPath, 'w') as fp:
            fp.write("[%s]\n" % GCAM_SECTION)
            fp.write(_UserDefaults)

    return _ConfigParser


def getParam(varName):
    return _ConfigParser.get(GCAM_SECTION, varName)


def getParamAsBoolean(varName):
    value = getParam(varName)
    return value and str(value).lower() in ('true', 'yes', 'on', '1') # added str() to avoid spurious error message


def ensureCSV(filename):
    'Add a .csv extension to a filename, if there is none. Return the filename.'
    mainPart, extension = os.path.splitext(filename)
    if not extension:
        filename = mainPart + '.csv'

    return filename


def ensureXLSX(filename):
    '''Force an extension of ".xlsx" on a filename'''
    xlsx = '.xlsx'

    mainPart, extension = os.path.splitext(filename)
    if not extension:
        filename = mainPart + xlsx
    elif extension != xlsx:
        filename += xlsx

    return filename


def getTempFile(suffix, text=True):
    from tempfile import mkstemp

    tmpDir = getParam('GCAM.TempDir') or "/tmp"
    fd, tmpFile = mkstemp(suffix=suffix, dir=tmpDir, text=text)
    os.close(fd)    # we don't need this
    return tmpFile


def readRegionMap(filename):
    '''
    Read a region map file and return the contents as a dictionary with each
    key equal to a standard GCAM region and each value being the region to
    map the original to (which can be an existing GCAM region or a new name.)
    '''
    import re
    mapping = {}
    pattern = re.compile('\t+')

    print "Reading region map '%s'" % filename
    with open(filename) as f:
        lines = f.readlines()

    for line in lines:
        line = line.strip()
        if line[0] == '#':
            continue

        tokens = pattern.split(line)
        #print "Line: '%s', tokens: %s" % (line, tokens)
        assert len(tokens) == 2, "Badly formatted line in region map '%s': %s" % (filename, line)

        mapping[tokens[0]] = tokens[1]

    return mapping


#
# TBD: bundle some of these functions into a "GcamResults" class
#
def limitYears(df, years):
    'Return the DF, stripping years outside the given limits'
    first, last = map(int, years)
    yearCols  = map(int, filter(str.isdigit, df.columns))
    dropYears = map(str, filter(lambda y: y < first or y > last, yearCols))
    df.drop(dropYears, axis=1, inplace=True)


def dropExtraCols(df, inplace=True):
    columns = df.columns
    unnamed = 'Unnamed:'    # extra (empty) columns can sneak in; eliminate them
    dropCols = filter(lambda s: s[0:len(unnamed)] == unnamed, columns)

    unneeded  = set(['scenario', 'Notes', 'Date'])
    columnSet = set(columns)
    dropCols += columnSet & unneeded    # drop any columns in both sets

    resultDF = df.drop(dropCols, axis=1, inplace=inplace)
    return resultDF


def interpolateYears(df, startYear=0):
    '''
    Interpolate linearly between each pair of years in the GCAM output. The
    timestep is calculated from the numerical (string) column headings given
    in the dataframe, which are assumed to represent years in the timeseries.
    If startYear is given, begin interpolation at this year.
    '''
    yearCols = filter(str.isdigit, df.columns)
    years = map(int, yearCols)

    for i in range(0, len(years)-1):
        start = years[i]
        end   = years[i+1]
        timestep = end - start

        if timestep == 1:       # don't interpolate annual results (LUC emissions, forcing)
            continue

        startCol = df[str(start)]
        endCol   = df[str(end)]

        # compute vector of annual deltas for each row
        delta = (endCol - startCol)/timestep

        # interpolate the whole column -- but don't interpolate before the start year
        for j in range(1, timestep):
            nextYear = start + j
            df[str(nextYear)] = df[str(nextYear-1)] + (0 if nextYear < startYear else delta)

    yearCols = filter(str.isdigit, df.columns)  # get annualized year columns
    years = map(int, yearCols)       # sort as integers
    years.sort()
    yearCols = map(str, years)       # convert back to strings, now sorted

    nonYearCols = list(set(df.columns) - set(yearCols))
    result = df.reindex_axis(nonYearCols + yearCols, axis=1, copy=True)
    return result

FT_DIESEL_MJ_PER_GAL   = 130.4
FAME_MJ_PER_GAL        = 126.0
ETOH_MJ_PER_GAL        = 81.3
BIOGASOLINE_MJ_PER_GAL = 122.3     # from GREET1_2011

FuelDensity = {
    'fame'          : FAME_MJ_PER_GAL,
    'bd'            : FAME_MJ_PER_GAL,
    'biodiesel'     : FAME_MJ_PER_GAL,          # GCAM name

    'ethanol'            : ETOH_MJ_PER_GAL,
    'etoh'               : ETOH_MJ_PER_GAL,
    'corn ethanol'       : ETOH_MJ_PER_GAL,     # GCAM name
    'cellulosic ethanol' : ETOH_MJ_PER_GAL,     # GCAM name
    'sugar cane ethanol' : ETOH_MJ_PER_GAL,     # GCAM name

    'ft'            : FT_DIESEL_MJ_PER_GAL,
    'FT biofuels'   : FT_DIESEL_MJ_PER_GAL,     # GCAM name

    'biogasoline'   : BIOGASOLINE_MJ_PER_GAL,
    'bio-gasoline'  : BIOGASOLINE_MJ_PER_GAL
}

def fuelDensityMjPerGal(fuelname):
    '''
    Return the fuel energy density of the named fuel.
    :param fuelname: the name of a fuel (currently must be one of {fame,bd,ethanol,etoh,biogasoline,bio-gasoline}.
    :return: energy density (MJ/gal)
    '''
    return FuelDensity[fuelname.lower()]
