# README #

The "gcam-utils" package offers a few Python scripts that are meant to helpful when working with 
GCAM. They resulted from my attempts to streamline my workflow with the model, but have been 
written as generally as possible. Hopefully other will find them equally useful.

### How do I get set up? ###

* More detailed information is available on this site's Wiki.
* Until I develop a proper installer, just download the files into a directory of your choice
  and add that directory to your shell's PATH variable so the scripts are found. 
* The first time you run any of the scripts (queueGCAM.py, csvDiff.py, or batchQuery.py),
  a stub configuration file is created in ~/.gcam.cfg. The help messages ("-h" flag to any of
  the scripts) describe which parameters you can set in the config file to alter defaults. In
  most cases, the defaults can be overridded using command-line arguments.
* These scripts were developed and tested on three machines, but hopefully run properly elsewhere: 
    * OS X 10.9.5 (Python 2.7.9, Anaconda distribution 2.0.1)
	* U. of Maryland's evergreen cluster (Linux, Python 2.7.8)
	* NERSC's carver cluster (Linux, Python 2.7.8, Anaconda distribution 2.1.0)
* The only non-standard Python module required is pandas, and it is used only in csvDiff.py.
* Unit tests are not yet available.

### Contribution guidelines ###

Contributions are welcome. These might include:

* Bug fixes
* Enhancements (and enhancement requests, but no promises on turn-around time!)
* Writing tests
* Code review